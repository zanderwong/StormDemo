package com.wander.test.storm;

import com.wander.test.storm.bolt.BoltBuilder;
import com.wander.test.storm.bolt.HBaseStoreBolt;
import com.wander.test.storm.bolt.PrintBolt;
import com.wander.test.storm.commons.conf.Configurer;
import com.wander.test.storm.spout.SpoutBuilder;
import org.apache.log4j.Logger;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.kafka.KafkaSpout;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;

public class MainTopology {


    private static Logger logger = Logger.getLogger(MainTopology.class);

    public static void main(String[] args) {
        MainTopology mainTopology = new MainTopology();
        mainTopology.submitTopology(args);

    }

    private void submitTopology(String[] args) {

        System.setProperty("HADOOP_USER_NAME", Configurer.getString("hadoop.user"));
        TopologyBuilder builder = new TopologyBuilder();
        KafkaSpout kafkaSpout = new SpoutBuilder().buildKafkaSpout();
        HBaseStoreBolt hBaseStoreBolt = new BoltBuilder().buildHBaseStoreBolt();

        String kafkaSpoutID = Configurer.getString("spout.kafka.id");
        int kafkaSpoutCount = Configurer.getInteger("spout.kafka.count");
        builder.setSpout(kafkaSpoutID, kafkaSpout, kafkaSpoutCount);

        String hbaseStoreBoltID = Configurer.getString("bolt.hbase.id");
        int hbaseStoreBoltCount = Configurer.getInteger("bolt.hbase.count");
        builder.setBolt(hbaseStoreBoltID, hBaseStoreBolt, hbaseStoreBoltCount).shuffleGrouping(kafkaSpoutID);

        builder.setBolt(Configurer.getString("bolt.print.id"), new PrintBolt(), 1).shuffleGrouping(kafkaSpoutID);


//        builder.setBolt("simpleHbaseBolt", new BoltBuilder().buildHBaseBolt("id","Concurrency",new Fields("nonBlocking")),3).fieldsGrouping("dbboltid", "language",new Fields("id", "name", "language"));



        Config conf = new Config();
        conf.setDebug(false);

        String topologyName = Configurer.getString("topology");

        if (args != null && args.length > 0) {
            logger.info("----------   Starting CloudCluster !   ----------  ");
            conf.setNumWorkers(6);
            try {
                StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
            } catch (AlreadyAliveException e) {
                e.printStackTrace();
            } catch (InvalidTopologyException e) {
                e.printStackTrace();
            } catch (AuthorizationException e) {
                e.printStackTrace();
            }
        } else {
            logger.info("----------   Starting LocalCluster !   ----------  ");
            conf.setMaxTaskParallelism(4);
            LocalCluster cluster = new LocalCluster();
            cluster.submitTopology(topologyName, conf, builder.createTopology());
//            cluster.shutdown();
        }
    }


}
