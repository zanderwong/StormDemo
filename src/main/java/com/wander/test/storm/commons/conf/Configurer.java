package com.wander.test.storm.commons.conf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * Configurer
 * Created by wander on 03th.May.2017.
 */

public class Configurer {
    private static Properties properties = new Properties();
    private static Logger logger = LoggerFactory.getLogger(Configurer.class);

    static {
        try {
            properties.load(Configurer.class.getResourceAsStream("/config.properties"));
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    public static String getString(String configFile, String key) {
        try {
            properties.load(Configurer.class.getClassLoader().getResourceAsStream(configFile));
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return properties.getProperty(key);
    }

    public static String getString(String key) {
        return properties.getProperty(key);
    }

    public static int getInteger(String configFile, String key) {
        try {
            properties.load(Configurer.class.getClassLoader().getResourceAsStream(configFile));
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return Integer.parseInt(properties.getProperty(key));
    }

    public static int getInteger(String key) {
        return Integer.parseInt(properties.getProperty(key));
    }

}
