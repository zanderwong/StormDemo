package com.wander.test.storm.commons.utils;

import com.google.gson.Gson;
import com.wander.test.storm.commons.jsn.JsonBean;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class StrUtils {

    private static Logger logger = LoggerFactory.getLogger(StrUtils.class);
    private static Gson gson = new Gson();

    public static String tmGen() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(new Date());
    }

    @Deprecated
    public static boolean isValidGson(String jstr) {
        try {
            gson.fromJson(jstr, JsonBean.class);
            return true;
        } catch (com.google.gson.JsonSyntaxException e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    public static boolean isValidGson(String jstr, Class clazz) {
        try {
            gson.fromJson(jstr, clazz);
            return true;
        } catch (com.google.gson.JsonSyntaxException e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    public static String getKafkaKey(Tuple tuple) {
        String key = tuple.getStringByField("key");

        if (isValidStrVal(key)) {
            return key;
        }

        throw new UnsupportedOperationException("kafka key is not valid");
    }

    public static boolean isValidStrVal(String str) {
        return str != null && str.length() > 0;
    }

}
