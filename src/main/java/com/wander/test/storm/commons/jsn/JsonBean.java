package com.wander.test.storm.commons.jsn;

public class JsonBean {
    public String id;
    public String name;
    public String language;
    public int birth;
    public Concurrency concurrency;

    public static class Concurrency {

        public String category;
        public boolean nonBlocking;
        public String easyOfUse;



    }
}
