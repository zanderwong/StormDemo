package com.wander.test.storm.commons.utils;

import com.wander.test.storm.commons.jsn.JsonBean;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;

public class HbaseUtils {

    public static void add(Put put, JsonBean data) {
        if (data.concurrency != null) {
            put.addColumn(Bytes.toBytes("Concurrency"), Bytes.toBytes("category"), Bytes.toBytes(data.concurrency.category));
            put.addColumn(Bytes.toBytes("Concurrency"), Bytes.toBytes("nonBlocking"), Bytes.toBytes(data.concurrency.nonBlocking));
            put.addColumn(Bytes.toBytes("Concurrency"), Bytes.toBytes("easyOfUse"), Bytes.toBytes(data.concurrency.easyOfUse));
        }
        put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("name"), Bytes.toBytes(data.name));
        put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("language"), Bytes.toBytes(data.language));
        put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("birth"), Bytes.toBytes(data.birth));

        String insTime = StrUtils.tmGen();
        put.addColumn(Bytes.toBytes("time"), Bytes.toBytes("insTime"), Bytes.toBytes(insTime));
    }
}


