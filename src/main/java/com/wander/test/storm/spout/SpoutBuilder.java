package com.wander.test.storm.spout;

import com.wander.test.storm.commons.conf.Configurer;
import org.apache.storm.kafka.*;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * SpoutBuilder
 * Created by wander on 19th.Apr.2017
 */
public class SpoutBuilder {

    public KafkaSpout buildKafkaSpout() {

        BrokerHosts brokerHosts = new ZkHosts(Configurer.getString("spout.kafka.zkHost"));
        String topic = Configurer.getString("spout.kafa.topic");
        String zkRoot = Configurer.getString("spout.kafka.zkRoot");
        String clientId = Configurer.getString("spout.kafka.clientId");
        SpoutConfig spoutConfig = new SpoutConfig(brokerHosts, topic, zkRoot, clientId);


        spoutConfig.scheme = new KeyValueSchemeAsMultiScheme(new KafkaKeyMsgScheme());
        spoutConfig.zkServers = new ArrayList<String>(Arrays.asList(Configurer.getString("zookeeper.servers").split(",")));
        spoutConfig.zkPort = Configurer.getInteger("zookeeper.port");

        //忽略zookeeper中记录的Kafka offset
//        spoutConfig.ignoreZkOffsets = true;
        spoutConfig.bufferSizeBytes = 1024;
        spoutConfig.fetchMaxWait = 1;
        spoutConfig.fetchSizeBytes = 1024 * 100;
        spoutConfig.stateUpdateIntervalMs = 1000;
        spoutConfig.startOffsetTime = kafka.api.OffsetRequest.LatestTime();

        return new KafkaSpout(spoutConfig);
    }
}
