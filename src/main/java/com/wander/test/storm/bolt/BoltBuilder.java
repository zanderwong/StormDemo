package com.wander.test.storm.bolt;

import com.wander.test.storm.commons.conf.Configurer;
import org.apache.storm.hbase.bolt.HBaseBolt;
import org.apache.storm.hbase.bolt.mapper.SimpleHBaseMapper;
import org.apache.storm.tuple.Fields;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * BoltBuilder
 * Created by wander on 21th.Apr.2017
 */
public class BoltBuilder {

    private static String tableName = Configurer.getString("hbase.table");
    private Properties config;

    public BoltBuilder(Properties config) {
        this.config = config;
    }

    public BoltBuilder() {
    }



    public HBaseStoreBolt buildHBaseStoreBolt() {
        return new HBaseStoreBolt();
    }

    public Map<String, Object> getHBaseConfig() {
        Map<String, Object> hbConfig = new HashMap<String, Object>();
        hbConfig.put("hbase.rootdir", Configurer.getString("hbase.rootdir"));
        hbConfig.put("hbase.zookeeper.quorum", Configurer.getString("hbase.zookeeper.quorum"));
        hbConfig.put("hbase.zookeeper.property.clientPort", Configurer.getInteger("hbase.zookeeper.property.clientPort"));
        return hbConfig;
    }

    public HBaseBolt buildHBaseBolt(String rowKey, String family, Fields qualifiers) {
        SimpleHBaseMapper mapper = new SimpleHBaseMapper()
                .withRowKeyField(rowKey)
                .withColumnFamily(family)
                .withColumnFields(qualifiers);
        return new HBaseBolt(tableName, mapper)
                .withBatchSize(Configurer.getInteger("hbase.batch.size"))
                .withConfigKey("hbConfig");
    }
}
