package com.wander.test.storm.bolt;


import com.google.gson.Gson;
import com.wander.test.storm.commons.conf.Configurer;
import com.wander.test.storm.commons.jsn.JsonBean;
import com.wander.test.storm.commons.utils.HbaseUtils;
import com.wander.test.storm.commons.utils.StrUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

/**
 * HBaseStoreBolt
 * Created by root on 21th.Nov.2017.
 */
public class HBaseStoreBolt extends BaseBasicBolt {


    private static Configuration HBASE_CONFIG;
    private static Logger logger = LoggerFactory.getLogger(HBaseStoreBolt.class);

    static {
        HBASE_CONFIG = HBaseConfiguration.create();
        HBASE_CONFIG.addResource(new Path("/src/main/resources/hbase-site.xml"));
        HBASE_CONFIG.set("hbase.rootdir", "hdfs://10.2.5.203/hbase");
    }

    private Connection connection;
    private Table table;


    @Override
    public void prepare(Map stormConf, TopologyContext context) {
        super.prepare(stormConf, context);
        try {
            System.setProperty("HADOOP_USER_NAME", "greejsj");
            connection = ConnectionFactory.createConnection(HBASE_CONFIG);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void execute(Tuple tuple, BasicOutputCollector basicOutputCollector) {
        System.out.println("tuple: "+tuple);
        String key = "";

        try {
            key = StrUtils.getKafkaKey(tuple);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }

        if (key.equals(Configurer.getString("kafka.topic.key"))) {
            String msg = tuple.getStringByField("value");

            if (StrUtils.isValidGson(msg, JsonBean.class)) {
                String table = Configurer.getString("hbase.table");
                JsonBean data = (JsonBean) decode(msg, JsonBean.class);
                String name = get(table, data.id, "info", "name");

                if (name == null || name.isEmpty()) {
                    logger.info("kafka message content: {}", msg);
                    put(data);
                    basicOutputCollector.emit(new Values(msg));

                }
            }

        } else {
            logger.error("WRONG KAFKA KEY DETECTED!");
        }
    }

    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("msg"));
    }

    private void put(JsonBean data) {
        try {
            table = connection.getTable(TableName.valueOf(Configurer.getString("hbase.table")));
            Put put = new Put(Bytes.toBytes(data.id));
            HbaseUtils.add(put, data);
            table.put(put);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String get(String tableName, String rowKey, String family, String qualifier) {
        String value = null;
        try {
            table = connection.getTable(TableName.valueOf(tableName));
            Get getRow = new Get(Bytes.toBytes(rowKey));
            Result result = table.get(getRow);
            value = Bytes.toString(result.getValue(Bytes.toBytes(family), Bytes.toBytes(qualifier)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return value;
    }

    private Object decode(String jstr, Class clazz) {
        Gson gson = new Gson();
        return gson.fromJson(jstr, clazz);
    }

    @Override
    public void cleanup() {
        super.cleanup();
        try {
            table.close();
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

